package com.shortcircuit.gsrestrict;

/**
 * @author ShortCircuit908
 *         Created on 3/13/2017.
 */
public enum Restriction {
	USE_L,
	USE_R,
	PLACE,
	BREAK,
	HAVE,
	HOLD,
	DROP,
	PICKUP
}
