package com.shortcircuit.gsrestrict;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author ShortCircuit908
 *         Created on 3/14/2017.
 */
public class AddCommand implements CommandExecutor {
	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (!(src instanceof Player)) {
			src.sendMessage(Text.builder().color(TextColors.RED).append(Text.of("This command is player-only")).build());
			return CommandResult.success();
		}
		Player player = (Player) src;
		String types = args.<String>getOne("restriction_types").get();
		Optional<Boolean> ignore_metadata_optional = args.getOne("ignore_metadata");
		boolean ignore_metadata = ignore_metadata_optional.isPresent() && ignore_metadata_optional.get();
		String[] split_types = types.split(",");
		Restriction[] restrictions = new Restriction[split_types.length];
		for (int i = 0; i < split_types.length; i++) {
			try {
				restrictions[i] = Restriction.valueOf(split_types[i].toUpperCase());
			}
			catch (Exception e) {
				player.sendMessage(Text.builder().color(TextColors.RED).append(Text.of("Unknown restriction: " + split_types[i])).build());
				player.sendMessage(Text.builder().color(TextColors.RED).append(Text.of("Possible values are: " + Arrays.toString(Restriction.values()))).build());
				return CommandResult.success();
			}
		}
		Optional<ItemStack> hand = player.getItemInHand(HandTypes.MAIN_HAND);
		if (!hand.isPresent()) {
			player.sendMessage(Text.builder().color(TextColors.RED).append(Text.of("You are not holding an item")).build());
			return CommandResult.success();
		}
		ItemStack item = hand.get();
		String id = GSRestrict.formatTypeWithProperties(item.getItem(), ignore_metadata ? null : item).toLowerCase();
		GSRestrict.RESTRICTIONS.put(id, restrictions);
		player.sendMessage(
				Text.builder().append(
						Text.builder()
								.color(TextColors.AQUA)
								.append(Text.of("Set restrictions of "))
								.build(),
						Text.builder().color(TextColors.YELLOW)
								.append(Text.of(id))
								.build(),
						Text.builder()
								.color(TextColors.AQUA)
								.append(Text.of(" to "))
								.build(),
						Text.builder()
								.color(TextColors.YELLOW)
								.append(Text.of(Arrays.toString(restrictions)))
								.build())
						.build()
		);
		return CommandResult.success();
	}
}
