package com.shortcircuit.gsrestrict; /**
 * @author ShortCircuit908
 * Created on 3/13/2017.
 */

import com.google.gson.reflect.TypeToken;
import com.shortcircuit.gsrestrict.json.JsonConfig;
import org.spongepowered.api.CatalogType;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataSerializable;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingEvent;
import org.spongepowered.api.event.item.inventory.ChangeInventoryEvent;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.event.item.inventory.InteractItemEvent;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.entity.PlayerInventory;
import org.spongepowered.api.item.inventory.transaction.SlotTransaction;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Plugin(id = GSRestrict.PLUGIN_IDENTIFIER, name = "Golden Sands Restrictions", version = "1.0", description = "Comprehensive item restriction")
public class GSRestrict {
	public static final HashMap<String, Restriction[]> RESTRICTIONS = new HashMap<>();
	public static final Type RESTRICTIONS_TYPE = new TypeToken<HashMap<String, Restriction[]>>() {
	}.getType();
	public static final String PLUGIN_IDENTIFIER = "gsrestrict";
	private static final File plugin_dir = new File("gsrestrict");

	static {
		plugin_dir.mkdirs();
	}

	private JsonConfig config;

	@Listener
	public void onServerStart(GameStartedServerEvent event) {
		File config_file = new File(plugin_dir + File.separator + "RESTRICTIONS.com.shortcircuit.gsrestrict.json");
		config = new JsonConfig(config_file);

		loadConfig();

		Task task = Sponge.getScheduler().createTaskBuilder().async().intervalTicks(20).delayTicks(20).execute(new Runnable() {
			@Override
			public void run() {
				for (Player player : Sponge.getServer().getOnlinePlayers()) {
					PlayerInventory inventory = (PlayerInventory) player.getInventory();
					Iterable<Inventory> slots = inventory.slots();
					for (Inventory slot_i : slots) {
						Slot slot = (Slot) slot_i;
						Optional<ItemStack> item_optional = slot.peek();
						if (item_optional.isPresent()) {
							ItemStack item = item_optional.get();
							Restriction[] item_restrictions = getRestrictions(item.getItem(), item);

							boolean restricted = hasRestriction(item_restrictions, Restriction.HAVE);

							boolean can_override = player.hasPermission(getOverridePermission(item.getItem(), Restriction.HAVE));

							if (restricted && !can_override) {
								slot.clear();
							}
						}
					}

				}
			}
		}).submit(this);
		System.out.println("com.shortcircuit.gsrestrict.GSRestrict scanner task started");

		CommandSpec command_spec_add = CommandSpec.builder()
				.description(Text.of("com.shortcircuit.gsrestrict.Restriction command"))
				.permission((PLUGIN_IDENTIFIER + ".command.add").toLowerCase())
				.executor(new AddCommand())
				.arguments(
						GenericArguments.onlyOne(GenericArguments.string(Text.of("restriction_types"))),
						GenericArguments.optional(GenericArguments.bool(Text.of("ignore_metadata")))
				)
				.build();

		Sponge.getCommandManager().register(this, command_spec_add, "addrestrict", "restrict");

		CommandSpec command_spec_remove = CommandSpec.builder()
				.description(Text.of("com.shortcircuit.gsrestrict.Restriction command"))
				.permission((PLUGIN_IDENTIFIER + ".command.remove").toLowerCase())
				.executor(new RemoveCommand())
				.arguments(
						GenericArguments.optional(GenericArguments.bool(Text.of("ignore_metadata")))
				)
				.build();

		Sponge.getCommandManager().register(this, command_spec_remove, "removerestrict", "unrestrict");

		CommandSpec command_spec_config = CommandSpec.builder()
				.description(Text.of("com.shortcircuit.gsrestrict.Restriction configuration"))
				.permission((PLUGIN_IDENTIFIER + ".command.config").toLowerCase())
				.executor(new ConfigCommand(this))
				.arguments(
						GenericArguments.onlyOne(GenericArguments.enumValue(Text.of("action"), ConfigCommand.Action.class))
				)
				.build();

		Sponge.getCommandManager().register(this, command_spec_config, "restrictconfig");
	}

	public void loadConfig() {
		config.loadConfig();
		RESTRICTIONS.clear();
		RESTRICTIONS.putAll(config.getNode("RESTRICTIONS", RESTRICTIONS_TYPE, new HashMap<>()));
	}

	public void saveConfig() {
		config.setNode("RESTRICTIONS", RESTRICTIONS);
		config.saveConfig();
	}

	@Listener
	public void onServerStop(GameStoppingEvent event) {
		saveConfig();
	}

	@Listener
	public void onInteractPrimary(final InteractBlockEvent.Primary event) {
		Optional<Player> optional_player = event.getCause().first(Player.class);

		if (!optional_player.isPresent()) {
			return;
		}

		Player player = optional_player.get();
		BlockSnapshot block = event.getTargetBlock();

		Restriction[] item_restrictions = getRestrictions(block.getState().getType(), block);

		boolean restricted = hasRestriction(item_restrictions, Restriction.USE_L);

		boolean can_override = player.hasPermission(getOverridePermission(block.getState().getType(), Restriction.USE_L));

		if (restricted && !can_override) {
			event.setCancelled(true);
		}
	}

	@Listener
	public void onInteractSecondary(final InteractBlockEvent.Secondary event) {
		Optional<Player> optional_player = event.getCause().first(Player.class);

		if (!optional_player.isPresent()) {
			return;
		}

		Player player = optional_player.get();
		BlockSnapshot block = event.getTargetBlock();

		Restriction[] item_restrictions = getRestrictions(block.getState().getType(), block);

		boolean restricted = hasRestriction(item_restrictions, Restriction.USE_R);

		boolean can_override = player.hasPermission(getOverridePermission(block.getState().getType(), Restriction.USE_R));

		if (restricted && !can_override) {
			event.setCancelled(true);
		}
	}

	@Listener
	public void onInteractPrimary(final InteractItemEvent.Primary event) {
		Optional<Player> optional_player = event.getCause().first(Player.class);

		if (!optional_player.isPresent()) {
			return;
		}

		Player player = optional_player.get();
		ItemStackSnapshot item = event.getItemStack();

		Restriction[] item_restrictions = getRestrictions(item.getType(), item);

		boolean restricted = hasRestriction(item_restrictions, Restriction.USE_L);

		boolean can_override = player.hasPermission(getOverridePermission(item.getType(), Restriction.USE_L));

		if (restricted && !can_override) {
			event.setCancelled(true);
		}
	}


	@Listener
	public void onInteractSecondary(final InteractItemEvent.Secondary event) {
		Optional<Player> optional_player = event.getCause().first(Player.class);

		if (!optional_player.isPresent()) {
			return;
		}

		Player player = optional_player.get();
		ItemStackSnapshot item = event.getItemStack();

		Restriction[] item_restrictions = getRestrictions(item.getType(), item);

		boolean restricted = hasRestriction(item_restrictions, Restriction.USE_R);

		boolean can_override = player.hasPermission(getOverridePermission(item.getType(), Restriction.USE_R));

		if (restricted && !can_override) {
			event.setCancelled(true);
		}
	}

	@Listener
	public void onDrop(DropItemEvent.Pre event) {
		Optional<Player> optional_player = event.getCause().first(Player.class);

		if (!optional_player.isPresent()) {
			return;
		}

		Player player = optional_player.get();

		for (ItemStackSnapshot item : event.getDroppedItems()) {
			Restriction[] item_restrictions = getRestrictions(item.getType(), item);

			boolean restricted = hasRestriction(item_restrictions, Restriction.DROP);

			boolean can_override = player.hasPermission(getOverridePermission(item.getType(), Restriction.DROP));

			if (restricted && !can_override) {
				event.setCancelled(true);
			}

			if (event.isCancelled()) {
				player.getInventory().offer(item.createStack());
			}
		}
	}

	@Listener
	public void onPlace(ChangeBlockEvent.Place event, @Root Player player) {
		for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {

			Restriction[] item_restrictions = getRestrictions(transaction.getFinal().getState().getType(), transaction.getFinal().getState());

			boolean restricted = hasRestriction(item_restrictions, Restriction.PLACE);

			boolean can_override = player.hasPermission(getOverridePermission(transaction.getFinal().getState().getType(), Restriction.PLACE));

			if (restricted && !can_override) {
				event.setCancelled(true);
				return;
			}
		}
	}

	@Listener
	public void onBreak(ChangeBlockEvent.Break event, @Root Player player) {
		for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
			Restriction[] item_restrictions = getRestrictions(transaction.getOriginal().getState().getType(), transaction.getOriginal().getState());

			boolean restricted = hasRestriction(item_restrictions, Restriction.BREAK);

			boolean can_override = player.hasPermission(getOverridePermission(transaction.getOriginal().getState().getType(), Restriction.BREAK));

			if (restricted && !can_override) {
				event.setCancelled(true);
				return;
			}
		}
	}

	@Listener
	public void onClick(ClickInventoryEvent event, @Root Player player) {
		List<SlotTransaction> transactions = event.getTransactions();
		for (SlotTransaction transaction : transactions) {
			Restriction[] item_restrictions = getRestrictions(transaction.getFinal().getType(), transaction.getFinal());

			boolean restricted = hasRestriction(item_restrictions, Restriction.HAVE);

			boolean can_override = player.hasPermission(getOverridePermission(transaction.getOriginal().getType(), Restriction.HAVE));

			if (restricted && !can_override) {
				event.setCancelled(true);
				return;
			}
		}

		Transaction<ItemStackSnapshot> cursor_transaction = event.getCursorTransaction();
		Restriction[] item_restrictions = getRestrictions(cursor_transaction.getFinal().getType(), cursor_transaction.getFinal());

		boolean restricted = hasRestriction(item_restrictions, Restriction.HAVE);

		boolean can_override = player.hasPermission(getOverridePermission(cursor_transaction.getOriginal().getType(), Restriction.HAVE));

		if (restricted && !can_override) {
			event.setCancelled(true);
		}
	}

	@Listener
	public void onHold(ChangeInventoryEvent.Held event, @Root Player player) {
		List<SlotTransaction> transactions = event.getTransactions();
		SlotTransaction transaction = event.getTransactions().get(1);
		//for (SlotTransaction transaction : transactions) {
		Restriction[] item_restrictions = getRestrictions(transaction.getOriginal().getType(), transaction.getOriginal());

		boolean restricted = hasRestriction(item_restrictions, Restriction.HOLD);

		boolean can_override = player.hasPermission(getOverridePermission(transaction.getOriginal().getType(), Restriction.HOLD));

		if (restricted && !can_override) {
			event.setCancelled(true);
			return;
		}
		//}
	}

	@Listener
	public void onPickup(ChangeInventoryEvent.Pickup event, @Root Player player) {
		List<SlotTransaction> transactions = event.getTransactions();
		for (SlotTransaction transaction : transactions) {
			Restriction[] item_restrictions = getRestrictions(transaction.getFinal().getType(), transaction.getFinal());

			boolean restricted = hasRestriction(item_restrictions, Restriction.PICKUP);

			boolean can_override = player.hasPermission(getOverridePermission(transaction.getOriginal().getType(), Restriction.PICKUP));

			if (restricted && !can_override) {
				event.setCancelled(true);
				return;
			}
		}
	}

	public static String getOverridePermission(CatalogType type, Restriction override) {
		return (PLUGIN_IDENTIFIER + ".override." + type.getId().replace(':', '_') + "." + override.name()).toLowerCase();
	}

	public static boolean hasRestriction(Restriction[] restrictions, Restriction check) {
		if (restrictions == null || restrictions.length == 0) {
			return false;
		}
		for (Restriction restriction : restrictions) {
			if (restriction == check) {
				return true;
			}
		}
		return false;
	}

	public static Optional<Integer> getRawMetadata(DataSerializable container) {
		return container.toContainer().getInt(DataQuery.of(';', "UnsafeDamage"));
	}


	public static String formatTypeWithProperties(CatalogType type, DataSerializable container) {
		StringBuilder builder = new StringBuilder(type.getId().toLowerCase());
		if (container != null) {
			Optional<Integer> metadata = getRawMetadata(container);
			metadata.ifPresent(integer -> builder.append(':').append(metadata.get()));
		}
		return builder.toString();
	}

	public static Restriction[] getRestrictions(CatalogType type, DataSerializable container) {
		String id = formatTypeWithProperties(type, container);
		Restriction[] item_restrictions = null;
		if (RESTRICTIONS.containsKey(id.replaceFirst(":\\d+", ""))) {
			item_restrictions = RESTRICTIONS.get(id.replaceFirst(":\\d+", ""));
		}
		if (RESTRICTIONS.containsKey(id)) {
			item_restrictions = RESTRICTIONS.get(id);
		}
		return item_restrictions;
	}
}