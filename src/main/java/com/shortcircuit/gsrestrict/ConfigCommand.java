package com.shortcircuit.gsrestrict;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

/**
 * @author ShortCircuit908
 *         Created on 3/14/2017.
 */
public class ConfigCommand implements CommandExecutor {
	private final GSRestrict plugin;

	public ConfigCommand(GSRestrict plugin) {
		this.plugin = plugin;
	}

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		Action action = args.<Action>getOne("action").get();
		switch (action){
			case LOAD:
				plugin.loadConfig();
				src.sendMessage(Text.builder().color(TextColors.AQUA).append(Text.of("Configuration loaded")).build());
				break;
			case SAVE:
				plugin.saveConfig();
				src.sendMessage(Text.builder().color(TextColors.AQUA).append(Text.of("Configuration saved")).build());
				break;
		}
		return CommandResult.success();
	}

	public static enum Action {
		SAVE,
		LOAD
	}
}
