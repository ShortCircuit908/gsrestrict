package com.shortcircuit.gsrestrict;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.LinkedList;
import java.util.Optional;

/**
 * @author ShortCircuit908
 *         Created on 3/14/2017.
 */
public class RemoveCommand implements CommandExecutor {
	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (!(src instanceof Player)) {
			src.sendMessage(Text.builder().color(TextColors.RED).append(Text.of("This command is player-only")).build());
			return CommandResult.success();
		}
		Player player = (Player) src;
		Optional<Boolean> ignore_metadata_optional = args.getOne("ignore_metadata");
		boolean ignore_metadata = ignore_metadata_optional.isPresent() && ignore_metadata_optional.get();
		Optional<ItemStack> hand = player.getItemInHand(HandTypes.MAIN_HAND);
		if (!hand.isPresent()) {
			player.sendMessage(Text.builder().color(TextColors.RED).append(Text.of("You are not holding an item")).build());
			return CommandResult.success();
		}
		ItemStack item = hand.get();
		String id = GSRestrict.formatTypeWithProperties(item.getItem(), ignore_metadata ? null : item).toLowerCase();
		if (!ignore_metadata) {
			GSRestrict.RESTRICTIONS.remove(id);
		}
		else {
			for (String key : new LinkedList<>(GSRestrict.RESTRICTIONS.keySet())) {
				if (key.toLowerCase().startsWith(id)) {
					GSRestrict.RESTRICTIONS.remove(key);
				}
			}
		}
		player.sendMessage(
				Text.builder().append(
						Text.builder()
								.color(TextColors.AQUA)
								.append(Text.of("Removed restrictions from "))
								.build(),
						Text.builder().color(TextColors.YELLOW)
								.append(Text.of(id))
								.build()
				).build()
		);
		return CommandResult.success();
	}
}
