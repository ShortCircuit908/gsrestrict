package com.shortcircuit.gsrestrict.json;

import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.reflect.TypeUtils;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A JSON-based configuration file
 *
 * @author ShortCircuit908
 */
public class JsonConfig {
	private final ClassLoader context;
	private final File file;
	private final String default_path;
	private final TreeMap<String, JsonElement> nodes = new TreeMap<>();
	private final LinkedList<String> comments = new LinkedList<>();
	private final HashMap<String, Object> value_cache = new HashMap<>();

	public JsonConfig(File file) {
		this(null, file, null);
	}

	public JsonConfig(Object context, File file, String default_path) {
		this.file = file;
		file.getParentFile().mkdirs();
		this.default_path = default_path;
		this.context = context == null ? null : context.getClass().getClassLoader();
		saveDefaultConfig();
		loadConfig();
	}

	/**
	 * Load the configuration state from the file, overwriting current values
	 */
	public synchronized void loadConfig() {
		synchronized (this) {
			try {
				if (!file.exists()) {
					throw new IllegalStateException("Configuration file not found");
				}
				StringBuilder builder = new StringBuilder();
				Scanner scanner = new Scanner(file);
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					builder.append(line).append('\n');
				}
				scanner.close();
				//TODO: Preserve location of comments
				Pattern comment_pattern = Pattern.compile("(/\\*((.|\n)*?)\\*/)|(//(.*?)\n)");
				String data = builder.toString();
				Matcher matcher = comment_pattern.matcher(data);
				while (matcher.find()) {
					comments.add(matcher.group());
				}
				data = matcher.replaceAll("");
				value_cache.clear();
				nodes.clear();
				HashMap<String, JsonElement> temp_nodes = JsonUtils.fromJson(data,
						new TypeToken<HashMap<String, JsonElement>>() {
						}.getType());
				if (temp_nodes == null) {
					temp_nodes = new HashMap<>();
				}
				nodes.putAll(temp_nodes);
			}
			catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Write the current configuration state to the file
	 */
	public synchronized void saveConfig() {
		synchronized (this) {
			try {
				file.createNewFile();
				FileWriter writer = new FileWriter(file);
				for (String comment : comments) {
					writer.write(comment + "\n");
				}
				writer.write(JsonUtils.toJsonString(nodes));
				writer.flush();
				writer.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Save the provided default configuration, without overwriting
	 */
	public synchronized void saveDefaultConfig() {
		synchronized (this) {
			saveDefaultConfig(false);
		}
	}

	/**
	 * Save the provided default configuration
	 *
	 * @param overwrite If <code>true</code>, any existing file will be overwritten
	 */
	public synchronized void saveDefaultConfig(boolean overwrite) {
		synchronized (this) {
			if (file.exists() && !overwrite) {
				return;
			}
			try {
				file.createNewFile();
				if (context != null && default_path != null) {
					Files.copy(context.getResourceAsStream(default_path), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
				}
				else {
					saveConfig();
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Set the value of a configuration node
	 *
	 * @param node  The node name
	 * @param value The value of the node
	 */
	public synchronized <T> void setNode(String node, T value) {
		synchronized (this) {
			nodes.put(node, JsonUtils.toJson(value));
		}
	}

	/**
	 * Get the value of a configuration node
	 * <p/>
	 * If the node does not exist, or if the value is null, the node's
	 * value is set to <code>default_if_null</code> and returned
	 *
	 * @param node            The node name
	 * @param type            The Java type of the node's value
	 * @param default_if_null The default value
	 */
	public <T> T getNode(String node, Type type, T default_if_null) {
		synchronized (this) {
			if (value_cache.containsKey(node)) {
				Object value = value_cache.get(node);
				if (value == null || TypeUtils.isAssignable(value.getClass(), type)) {
					return (T) value;
				}
			}
			T value = JsonUtils.fromJson(nodes.get(node), type);
			if (value == null && default_if_null != null) {
				value = default_if_null;
				setNode(node, default_if_null);
			}
			value_cache.put(node, value);
			return value;
		}
	}

	/**
	 * Get the value of a configuration node
	 * <p/>
	 * If the node does not exist, or if the value is null, the node's
	 * value is set to <code>default_if_null</code> and returned
	 *
	 * @param node            The node name
	 * @param type            The class of the node's value
	 * @param default_if_null The default value
	 */
	public <T> T getNode(String node, Class<T> type, T default_if_null) {
		return getNode(node, (Type) type, default_if_null);
	}
}
